const branch = process.env.CI_COMMIT_BRANCH

const config = {
    branches: [
        { name: 'master', prerelease: false},
        { name: 'development', prerelease: true }],
    plugins: [
        "@semantic-release/commit-analyzer",
        "@semantic-release/release-notes-generator",
        ["@semantic-release/exec",
            {
                verifyConditionsCmd: "./update-version.sh $(cat plugin.cfg | grep -oP '(?<=^VERSION=).+$')",
                prepareCmd: "./update-version.sh ${nextRelease.version} "
            }
        ]
    ],
    "npmPublish": false
}

if (branch == 'master') {
    config.plugins.push('@semantic-release/gitlab', '@semantic-release/changelog', [
        '@semantic-release/git',
        {
            assets: [
                "package.json",
                "package-lock.json",
                "CHANGELOG.md",
                "plugin.cfg",
                "release.cfg",
                "prerelease.cfg"],
            message: 'chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}',
        },
    ])
} else if (branch == 'development') {
    config.plugins.push([
        '@semantic-release/git',
        {
            assets: [
                "package.json",
                "package-lock.json",
                "plugin.cfg",
                "release.cfg",
                "prerelease.cfg"],
            message: 'chore(prerelease): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}',
        },
    ])
}

module.exports = config