## [1.0.12](https://gitlab.com/VVlasy/loxberry-control-my-spa/compare/v1.0.11...v1.0.12) (2020-12-21)


### Bug Fixes

* **CI:** fix condition in config ([f08887a](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/f08887a2f8fffaea38c9f29991d8fdbb7929bc1f))

## [1.0.10](https://gitlab.com/VVlasy/loxberry-control-my-spa/compare/v1.0.9...v1.0.10) (2020-12-21)


### Bug Fixes

* **CI:** proper staging for last fix ([4a3e24f](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/4a3e24f004e4758af68540118052da4a42d29542))
* **CI:** update release/prerelease version from plugin.cfg ([295e0d3](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/295e0d376f4a1d37151fc55611d08bea72346548))

## [1.0.9](https://gitlab.com/VVlasy/loxberry-control-my-spa/compare/v1.0.8...v1.0.9) (2020-12-21)


### Bug Fixes

* **CI:** cleanup ([fa20159](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/fa20159bc423a358fca3b812586179447871c27e))

## [1.0.8](https://gitlab.com/VVlasy/loxberry-control-my-spa/compare/v1.0.7...v1.0.8) (2020-12-21)


### Bug Fixes

* **CI:** improve logging ([1a72f40](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/1a72f407b26b2bcb94ce8fcf23d2bb7baf762584))

## [1.0.7](https://gitlab.com/VVlasy/loxberry-control-my-spa/compare/v1.0.6...v1.0.7) (2020-12-21)


### Bug Fixes

* **CI:** remove useless script ([a315fc0](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/a315fc07f9e765242f9c7bfd89520f7b3fc3e337))
* **CI:** update version script missing env var usage ([86d8f38](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/86d8f385dc87243830e610ad00e683f8c9fb1533))

## [1.0.6](https://gitlab.com/VVlasy/loxberry-control-my-spa/compare/v1.0.5...v1.0.6) (2020-12-21)


### Bug Fixes

* **CI:** fix permission error ([2187267](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/2187267bd3d3a9b8f41e4985f94a6279438b4927))
* **CI:** plugin staging ([4ca9a24](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/4ca9a249bcd5b9922d5c3173ba62e58f5af684e3))

## [1.0.5](https://gitlab.com/VVlasy/loxberry-control-my-spa/compare/v1.0.4...v1.0.5) (2020-12-21)


### Bug Fixes

* **CI:** do version update differently ([a09d958](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/a09d9584c08b543bf070f55498a6fa28c8ad196b))

## [1.0.4](https://gitlab.com/VVlasy/loxberry-control-my-spa/compare/v1.0.3...v1.0.4) (2020-12-21)


### Bug Fixes

* **CI:** try sudo ([f479398](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/f479398f5c7868308972dea5dcd9ba4c1f5b2f44))

## [1.0.3](https://gitlab.com/VVlasy/loxberry-control-my-spa/compare/v1.0.2...v1.0.3) (2020-12-20)


### Bug Fixes

* **CI:** fix putting branch ([475b2cb](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/475b2cb25fbe0ed08d5e5f84649964adcd33ce33))

## [1.0.2](https://gitlab.com/VVlasy/loxberry-control-my-spa/compare/v1.0.1...v1.0.2) (2020-12-20)


### Bug Fixes

* **CI:** fix syntax ([9c65dc8](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/9c65dc88adecfb1a12a28d533973535b292ae5c7))
* **CI:** put exec in proper place ([bd583bc](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/bd583bcc6d134f146d23b4bb295db873a399b087))

## [1.0.1](https://gitlab.com/VVlasy/loxberry-control-my-spa/compare/v1.0.0...v1.0.1) (2020-12-20)


### Bug Fixes

* **CI:** Add exec plugin ([5ef4098](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/5ef409830394d5228f433a3a0e913c114439aba7))
* **CI:** fix version update script ([afccfd5](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/afccfd593d053cd0497502fc4bc48b92f7cadf8e))
* **CI:** set correct plugin order ([29b47fc](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/29b47fcc58c2258f279da21b9d6a304feafa52d9))
* **CI:** set correct step to execute version update ([8b66283](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/8b66283985a7f801af027d2d947248908ebd771b))

# 1.0.0 (2020-12-20)


### Bug Fixes

* **CI:** add gitlab plugin ([3e4adbf](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/3e4adbf2c0c95661a66efeb77e6c86aac6d03856))
* **CI:** configure branches ([da36f50](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/da36f501c4a10bd6ea279504ec6b52f8128d1aab))
* **CI:** disable github plugin ([a6723fc](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/a6723fcc635449b28ac315ca83947856eb6c9676))
* **CI:** disable NPM publish ([7573073](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/7573073b342847644a3fec31047902bfb79fbe2b))
* **CI:** dont pack node_modules ([28d5ede](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/28d5ede656e71f41934130e0b7dbcdb605745e60))
* **CI:** semantic-release config ([eb977b7](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/eb977b798e7166ec0e395ee919b2abb28be77c2d))
* **CI:** use semantic-version for versioning ([bf805ac](https://gitlab.com/VVlasy/loxberry-control-my-spa/commit/bf805ac37cf311a085825fabc8a5a1d656d700ff))
